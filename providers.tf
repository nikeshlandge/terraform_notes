# provider block for aws cloud
provider "aws" {
  region = "us-east-1a"
  access_key = "12345"
  secret_key = "12345"
}

#provider block for azure cloud
# Configuring Multiple AWS providers
provider "aws" {
  region = "us-east-1a"
  profile = "dev-profile"
  access_key = "12345"
  secret_key = "12345"
}
provider "aws" {
  alias = "prod"
  region = "us-east-1a"
  access_key = "12345"
  secret_key = "12345"
}
#how to refer to these different providers while defining our resources in terraform templates.
resource "aws_s3_bucket" "my_bucket1" {
  bucket = "mybucket-name1"
  acl = "public-read"
}
resource "aws_s3_bucket" "my_bucket2" {
  provider = aws.prod
  bucket = "mybucket-name2"
  acl = "public-read"
}