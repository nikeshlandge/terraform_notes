# Terraform_Notes
## Provider block
A provider is a Terraform plugin that allows users to manage an external API. Provider plugins like the AWS provider or the cloud-init provider act as a translation layer that allows Terraform to communicate with many different cloud providers, databases, and services.
Terraform uses providers to provision resources, which describe one or more infrastructure objects like virtual networks and compute instances. Each provider on the Terraform Registry has documentation detailing available resources and their configuration options.
###### the Providers are- 
- aws provider.
- azure provider.
- Google provider.
### Configuring Multiple AWS providers
Now, let’s talk about how can we set up multiple AWS providers and use them for creating resources in both AWS accounts.

We can’t write two or more providers with the same name i.e. two AWS providers. If there’s any such need the terraform has provided a way to do that which is to use alias argument.

Note: A provider block without an alias argument is the default configuration for that provider.